let collection = [];

// Write the queue functions below.

function print() {
    //Print queue elements.
    return collection
}
// console.log(print())

function enqueue(name) {
    // collection.push(name)

    if(collection.length == 0){
        collection[0] = name
    }else{
        const index = collection.length
        collection[index] = name
    }
    return collection  
}


function dequeue(){

    // collection.shift()

    collection = collection.slice(1)

    return collection

    // let newArray = []

    // for (let i = 1; i < collection.length; i++){
    //     newArray[i - 1] = collection[i]
    // }

    // return newArray
}


function front(){
    return collection[0]
}

function size(){
    return collection.length
}

function isEmpty(){

   return collection.length == 0
   
}


module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};